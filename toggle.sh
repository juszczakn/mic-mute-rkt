#!/bin/sh

pacmd list-sources | grep '* index:' | awk '{ print $3 }' | xargs -I{} pactl set-source-mute {} toggle

echo 0 > /dev/shm/rkt-mic-mute.comm
