# Mic Mute Rkt

Allows toggling muting the active mic on Linux using pulseaudio via a GUI/shortcut.

# Requirements

Must have racket installed. Tested with 8.1 CS.

# Use

1. Clone the repo somewhere, like `~/opt/` (used in examples below).
1. Start the GUI with `racket ~/opt/main.rkt`
   (or copy the `.desktop` example file to `~/.local/share/applications/`, to have it show up in application menu.
   Note that you'll have to edit to it provide the right path).
1. Add a global keyboard shortcut for `~/opt/mic-mute-rkt/toggle.sh`.
   - On Ubuntu, search for `Keyboard Shortcuts`.
